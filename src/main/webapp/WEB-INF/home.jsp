<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>OnStage - Concerts near you</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="files/css/styles.css"/>
</head>
<body>
<header class="wave-container header">
    <div class="container">
        <h1>
            <span>
                Never miss a concert again
            </span>
            <span>
                OnStage for iOS and Android
            </span>
        </h1>
        <div class="screenshot">
            <img src="files/images/iphone_show_screen.png" alt="Screenshot of the OnStage app on an iPhone X."/>
        </div>
    </div>
    <div class="wave"></div>
</header>
<section class="register wave-container">
    <div class="container">
        <h2>
            Are you an artist?
        </h2>

        <div class="horizontal-list">
            <div class="item">
                <div class="card">
                    <svg style="width:42px;height:42px" viewBox="0 0 24 24">
                        <path fill="var(--ternary-lighter)"
                              d="M18,19H6V17.6C6,15.6 10,14.5 12,14.5C14,14.5 18,15.6 18,17.6M12,7A3,3 0 0,1 15,10A3,3 0 0,1 12,13A3,3 0 0,1 9,10A3,3 0 0,1 12,7M12,3A1,1 0 0,1 13,4A1,1 0 0,1 12,5A1,1 0 0,1 11,4A1,1 0 0,1 12,3M19,3H14.82C14.4,1.84 13.3,1 12,1C10.7,1 9.6,1.84 9.18,3H5A2,2 0 0,0 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5A2,2 0 0,0 19,3Z"/>
                    </svg>

                    <h3>Manage your artist profile</h3>
                    <p>
                        Add photos, social links and a bio for your fans
                        to find you everywhere easily.
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="card">
                    <svg style="width:42px;height:42px" viewBox="0 0 24 24">
                        <path fill="var(--ternary-lighter)"
                              d="M9,3A4,4 0 0,1 13,7H5A4,4 0 0,1 9,3M11.84,9.82L11,18H10V19A2,2 0 0,0 12,21A2,2 0 0,0 14,19V14A4,4 0 0,1 18,10H20L19,11L20,12H18A2,2 0 0,0 16,14V19A4,4 0 0,1 12,23A4,4 0 0,1 8,19V18H7L6.16,9.82C5.67,9.32 5.31,8.7 5.13,8H12.87C12.69,8.7 12.33,9.32 11.84,9.82M9,11A1,1 0 0,0 8,12A1,1 0 0,0 9,13A1,1 0 0,0 10,12A1,1 0 0,0 9,11Z"/>
                    </svg>

                    <h3>
                        Let your fans know where you'll perform
                    </h3>
                    <p>
                        Add your tours and your shows so your fans
                        always know when they can come see you live.
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="card">
                    <svg style="width:42px;height:42px" viewBox="0 0 24 24">
                        <path fill="var(--ternary-lighter)"
                              d="M12,5.5A3.5,3.5 0 0,1 15.5,9A3.5,3.5 0 0,1 12,12.5A3.5,3.5 0 0,1 8.5,9A3.5,3.5 0 0,1 12,5.5M5,8C5.56,8 6.08,8.15 6.53,8.42C6.38,9.85 6.8,11.27 7.66,12.38C7.16,13.34 6.16,14 5,14A3,3 0 0,1 2,11A3,3 0 0,1 5,8M19,8A3,3 0 0,1 22,11A3,3 0 0,1 19,14C17.84,14 16.84,13.34 16.34,12.38C17.2,11.27 17.62,9.85 17.47,8.42C17.92,8.15 18.44,8 19,8M5.5,18.25C5.5,16.18 8.41,14.5 12,14.5C15.59,14.5 18.5,16.18 18.5,18.25V20H5.5V18.25M0,20V18.5C0,17.11 1.89,15.94 4.45,15.6C3.86,16.28 3.5,17.22 3.5,18.25V20H0M24,20H20.5V18.25C20.5,17.22 20.14,16.28 19.55,15.6C22.11,15.94 24,17.11 24,18.5V20Z"/>
                    </svg>

                    <h3>
                        Reach new fans
                    </h3>
                    <p>
                        Your shows are shown to people close to them.
                        Grow your fanbase and reach more people than ever before.
                    </p>
                </div>
            </div>
        </div>
        <div class="center">
            <a href="signin" class="btn primary">
                Register as an artist
            </a>
        </div>
    </div>
    <div class="wave"></div>
</section>
<section class="screenshots">
    <div class="container">
        <h2>
            Are you looking for concerts?
        </h2>
        <div class="vertical-rows">
            <!-- show screen -->
            <div class="row">
                <div class="left">
                    <div class="card">
                        <h3>All you need to know in one place.</h3>
                        <p>
                            Find all the information you need about a show in one place.
                            You can easily see at what time the doors will open, which venue
                            the show will take place at and the price range of the tickets.
                        </p>
                    </div>
                </div>
                <div class="right">
                    <img src="files/images/iphone_show_screen.png" alt="Screenshot of the show screen on an iPhone X."/>
                </div>
            </div>
            <!-- artist screen -->
            <div class="row">
                <div class="left">
                    <img src="files/images/samsung_artist_screen.png"
                         alt="Screenshot of the artist screen on a Samsung Galaxy S10."/>
                </div>
                <div class="right">
                    <div class="card">
                        <h3>Connect with your idols.</h3>
                        <p>
                            Artist profiles give you easy access to an artist's social
                            media profile so you can connect with them further.
                        </p>
                    </div>
                </div>
            </div>

            <!-- Tour screen -->
            <div class="row">
                <div class="left">
                    <div class="card">
                        <h3>Need more shows? We've got them.</h3>
                        <p>
                            If you want to attend shows at different locations or check
                            out more shows from a tour, we've got you covered.
                            Easily view all the shows from a particular tour.
                        </p>
                    </div>
                </div>
                <div class="right">
                    <img src="files/images/iphone_tour_screen.png" alt="Screenshot of the tour screen on an iPhone X."/>
                </div>
            </div>

            <!-- Planned screen -->
            <div class="row">
                <div class="left">
                    <img src="files/images/samsung_planned_screen.png"
                         alt="Screenshot of the planned shows screen on a Samsung Galaxy S10."/>
                </div>
                <div class="right">
                    <div class="card">
                        <h3>Make plans and enjoy the music.</h3>
                        <p>
                            Once you're set, plan your shows and keep track
                            of all the shows you'll be attending. After that,
                            enjoy the music.
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<footer class="home">
    <div class="container">
        <div class="horizontal-list">
            <div class="item">
                <p>Made by Maxime Esnol</p>
            </div>
            <div class="item">
                <ul>
                    <li>
                        <a href="signin">
                            Artist sign in
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="files/js/scroller.js"></script>
<script>
    let leftCardAnimations = new ScrollWatcher({
        element: document.querySelectorAll("section.screenshots .vertical-rows .row .left"),
        animation: {
            name: "appearLeft",
            duration: "1000",
            fill_mode: "forwards"
        },
        trigger: 350
    });

    let rightCardAnimations = new ScrollWatcher({
        element: document.querySelectorAll("section.screenshots .vertical-rows .row .right"),
        animation: {
            name: "appearRight",
            duration: "1000",
            fill_mode: "forwards"
        },
        trigger: 350
    });

    let items = document.querySelectorAll("section.register .item");

    items.forEach((element, key) => {
        new ScrollWatcher({
            element: element,
            animation: {
                name: "popUp",
                duration: "500",
                fill_mode: "forwards",
                easing: "cubic-bezier(.43,1.84,.48,.71)"
            },
            trigger: "50",
            timeout: key*200
        });
    });
</script>
</body>
</html>
