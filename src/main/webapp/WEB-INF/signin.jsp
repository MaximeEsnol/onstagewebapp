<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 05/06/2020
  Time: 12:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>OnStage - Artist Signin</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="files/css/styles.css"/>
</head>
<body>
    <div class="signin-screen">
        <div class="content">
            <c:if test="${error != null}">
                <div class="dialog error">
                    <p>
                        <c:out value="${error}"/>
                    </p>
                </div>
            </c:if>
            <c:if test="${message != null}">
                <div class="dialog success">
                    <p>
                        <c:out value="${message}"/>
                    </p>
                </div>
            </c:if>
            <div class="forms">
                <form method="post" action="auth/register">
                    <h2>Register</h2>
                    <div class="input">
                        <label for="email">Your email address</label>
                        <input type="email" id="email" name="email" placeholder="example@domain.com"/>
                    </div>
                    <div class="input">
                        <label for="username">Your username</label>
                        <input type="text" id="username" name="username" placeholder="3 to 20 characters long and only letters and numbers"/>
                    </div>
                    <div class="input">
                        <label for="password">Your password</label>
                        <input type="password" id="password" name="password" placeholder="At least 8 characters, 1 number, 1 lowercase and 1 uppercase letter"/>
                    </div>
                    <div class="center" >
                        <button type="submit" class="btn primary">
                            Sign up
                        </button>
                    </div>
                </form>

                <form method="post" action="auth/signin">
                    <h2>Sign in</h2>
                    <div class="input">
                        <label for="signin-email">Your email address</label>
                        <input type="email" id="signin-email" name="signin-email" placeholder="example@domain.com"/>
                    </div>
                    <div class="input">
                        <label for="signin-password">Your password</label>
                        <input type="password" id="signin-password" name="signin-password" placeholder="At least 8 characters, 1 number, 1 lowercase and 1 uppercase letter"/>
                    </div>
                    <div class="center">
                        <button type="submit" class="btn primary">Sign in</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
