<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 07/06/2020
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit a show - OnStage</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../../files/css/styles.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
<body class="app">
<nav>
    <ul>
        <li>
            <a href="../../artist/dashboard">
                Dashboard home
            </a>
        </li>
        <li>
            <a href="../../artist/artist/new">
                Add artist
            </a>
        </li>
    </ul>
</nav>
<main>
    <c:if test="${error != null}">
        <div class="dialog error">
            <p>
                <c:out value="${error}"/>
            </p>
        </div>
    </c:if>
    <c:if test="${message != null}">
        <div class="dialog success">
            <p>
                <c:out value="${message}"/>
            </p>
        </div>
    </c:if>
    <h1>Edit show</h1>

    <form method="post">
        <fieldset>
            <legend>Location information</legend>

            <div class="input">
                <label for="location">
                    Location (city, country)
                </label>
                <input type="text" required id="location" name="location" value="<c:out value="${show.location}"/>"/>
            </div>
            <div class="input">
                <label for="venue">
                    Venue
                </label>
                <input type="text" required id="venue" name="venue" value="<c:out value="${show.venue}"/>"/>
            </div>
            <div class="input">
                <label for="coords">
                    Coords
                </label>
                <input type="text" required id="coords" name="coords" value="<c:out value="${show.coords}"/>" placeholder="Example: 52.5063175,13.4414429"/>
            </div>
        </fieldset>
        <fieldset>
            <legend>Date and time</legend>
            <div class="input">
                <label for="datetime">
                    Date and time of the show
                </label>
                <input type="text" id="datetime" name="datetime" required>
            </div>
        </fieldset>
        <fieldset>
            <legend>Tickets</legend>
            <div class="input">
                <label for="ticketing">
                    Buy link
                </label>
                <input type="url" id="ticketing" name="ticketing" value="<c:out value="${show.ticketing}"/>" required/>
            </div>
            <div class="input">
                <label for="currency">
                    Currency
                </label>
                <select id="currency" name="currency" required>
                    <option  <c:if test="${show.currency eq 'EUR'}">selected</c:if> value="EUR" selected>€ Euro</option>
                    <option  <c:if test="${show.currency eq 'USD'}">selected</c:if> value="USD">$ US Dollar</option>
                    <option  <c:if test="${show.currency eq 'GBP'}">selected</c:if> value="GBP">£ British Pound</option>
                </select>
            </div>
            <div class="input">
                <label for="min">
                    Minimum price
                </label>
                <input type="number" step="0.01" required min="0.00" name="min" value="<c:out value="${show.min}"/>" id="min"/>
            </div>

            <div class="input">
                <label for="max">
                    Maximum price
                </label>
                <input type="number" step="0.01" required min="0.00" name="max"  value="<c:out value="${show.max}"/>" id="max"/>
            </div>
        </fieldset>
        <button type="submit" class="btn primary">
            Save changes
        </button>
    </form>
</main>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr("#datetime", {
        enableTime: true,
        time_24hr: true,
        altInput: true,
        defaultDate: "<c:out value="${date}"/>",
        altFormat: "F j, Y H:i",
        dateFormat: "Y-m-d H:i",
        minDate: "today"
    });
</script>
</body>
</html>
