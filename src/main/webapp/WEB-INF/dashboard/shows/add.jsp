<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 07/06/2020
  Time: 13:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add new show - OnStage</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../../../../files/css/styles.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
<body class="app">
<nav>
    <ul>
        <li>
            <a href="../../../../artist/dashboard">
                Dashboard home
            </a>
        </li>
        <li>
            <a href="../../../../artist/artist/new">
                Add artist
            </a>
        </li>
    </ul>
</nav>
<main>
    <c:if test="${error != null}">
        <div class="dialog error">
            <p>
                <c:out value="${error}"/>
            </p>
        </div>
    </c:if>
    <h1>Add show to <c:out value="${tour.name}"/></h1>

    <form method="post">
        <fieldset>
            <legend>Location information</legend>

            <div class="input">
                <label for="location">
                    Location (city, country)
                </label>
                <input type="text" required id="location" name="location"/>
            </div>
            <div class="input">
                <label for="venue">
                    Venue
                </label>
                <input type="text" required id="venue" name="venue"/>
            </div>
            <div class="input">
                <label for="coords">
                    Coords
                </label>
                <input type="text" required id="coords" name="coords" placeholder="Example: 52.5063175,13.4414429"/>
            </div>
        </fieldset>
        <fieldset>
            <legend>Date and time</legend>
            <div class="input">
                <label for="datetime">
                    Date and time of the show
                </label>
                <input type="text" id="datetime" name="datetime" required>
            </div>
        </fieldset>
        <fieldset>
            <legend>Tickets</legend>
            <div class="input">
                <label for="ticketing">
                    Buy link
                </label>
                <input type="url" id="ticketing" name="ticketing" required/>
            </div>
            <div class="input">
                <label for="currency">
                    Currency
                </label>
                <select id="currency" name="currency" required>
                    <option value="EUR" selected>€ Euro</option>
                    <option value="USD">$ US Dollar</option>
                    <option value="GBP">£ British Pound</option>
                </select>
            </div>
            <div class="input">
                <label for="min">
                    Minimum price
                </label>
                <input type="number" step="0.01" required min="0.00" name="min" id="min"/>
            </div>

            <div class="input">
                <label for="max">
                    Maximum price
                </label>
                <input type="number" step="0.01" required min="0.00" name="max" id="max"/>
            </div>
        </fieldset>
        <button type="submit" class="btn primary">
            Upload show
        </button>
    </form>
</main>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr("#datetime", {
        enableTime: true,
        time_24hr: true,
        altInput: true,
        altFormat: "F j, Y H:i",
        dateFormat: "Y-m-d H:i",
        minDate: "today"
    });
</script>
</body>
</html>
