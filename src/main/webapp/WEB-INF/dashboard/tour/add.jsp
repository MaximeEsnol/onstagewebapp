<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 06/06/2020
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New tour for <c:out value="${artist.name}"/> - OnStage</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../../../files/css/styles.css"/>
</head>
<body class="app">
<nav>
    <ul>
        <li>
            <a href="../../../artist/dashboard">
                Dashboard home
            </a>
        </li>
        <li>
            <a href="../../../artist/artist/new">
                Add artist
            </a>
        </li>
    </ul>
</nav>
<main>
    <h1>New tour for <c:out value="${artist.name}"/></h1>

    <form method="post">
        <div class="input">
            <label for="name">
                Tour name
            </label>
            <input type="text" id="name" name="name" required/>
        </div>
        <div class="input">
            <label for="description">
                About this tour
            </label>
            <textarea name="description" id="description"></textarea>
        </div>

        <button type="submit" class="btn primary">
            Add tour
        </button>
    </form>
</main>
</body>
</html>
