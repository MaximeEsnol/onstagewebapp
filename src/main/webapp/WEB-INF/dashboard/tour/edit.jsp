<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 06/06/2020
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit <c:out value="${tour.name}"/> - OnStage</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../../files/css/styles.css"/>
</head>
<body class="app">
<nav>
    <ul>
        <li>
            <a href="../../artist/dashboard">
                Dashboard home
            </a>
        </li>
        <li>
            <a href="../../artist/artist/new">
                Add artist
            </a>
        </li>
    </ul>
</nav>
<main>
    <h1>Edit <c:out value="${tour.name}"/> by <c:out value="${tour.artist.name}"/></h1>
    <a class="btn primary" href="<c:out value="${tour.id}"/>/shows">
        Shows
    </a>
    <c:if test="${error != null}">
        <div class="dialog error">
            <p>
                <c:out value="${error}"/>
            </p>
        </div>
    </c:if>
    <c:if test="${message != null}">
        <div class="dialog success">
            <p>
                <c:out value="${message}"/>
            </p>
        </div>
    </c:if>
    <form method="post">
        <div class="input">
            <label for="name">
                Tour name
            </label>
            <input type="text" id="name" name="name" value="<c:out value="${tour.name}"/>" required/>
        </div>
        <div class="input">
            <label for="description">
                About this tour
            </label>
            <textarea name="description" id="description"><c:out value="${tour.description}"/></textarea>
        </div>
        <input type="hidden" name="artistId" id="artistId" value="<c:out value="${tour.artist.id}"/>" required />
        <button type="submit" class="btn primary">
            Save changes
        </button>

        <a href="<c:out value="${id}"/>/delete" class="btn danger">
            Delete
        </a>
    </form>
</main>
</body>
</html>
