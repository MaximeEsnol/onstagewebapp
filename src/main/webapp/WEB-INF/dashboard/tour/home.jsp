<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 06/06/2020
  Time: 16:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tours from <c:out value="${artist.name}"/> - OnStage</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../../files/css/styles.css"/>
</head>
<body class="app">
<nav>
    <ul>
        <li>
            <a href="../../artist/dashboard">
                Dashboard home
            </a>
        </li>
        <li>
            <a href="../../artist/artist/new">
                Add artist
            </a>
        </li>
    </ul>
</nav>
<main>
    <c:if test="${message != null}">
        <div class="dialog success">
            <p>
                <c:out value="${message}"/>
            </p>
        </div>
    </c:if>
    <h1>Tours from <c:out value="${artist.name}"/> </h1>
    <a href="../../artist/<c:out value="${id}"/>/tour/add" class="btn primary">
        Add tour
    </a>
    <c:forEach var="tour" items="${tours}">
        <div class="tour">
            <a href="../../artist/tour/${tour.id}">
                <h3><c:out value="${tour.name}"/> </h3>
                <p><c:out value="${fn:substring(tour.description, 0, 100)}"/>...</p>
            </a>
        </div>
    </c:forEach>
</main>
</body>
</html>
