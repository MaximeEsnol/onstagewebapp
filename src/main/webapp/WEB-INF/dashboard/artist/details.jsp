<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 06/06/2020
  Time: 14:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        <c:choose>
            <c:when test="${artist != null}">
                Manage <c:out value="${artist.name}"/> - OnStage
            </c:when>
            <c:otherwise>
                <c:if test="${error != null}">
                    No access - OnStage
                </c:if>
            </c:otherwise>
        </c:choose>
    </title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../files/css/styles.css"/>
</head>
<body class="app">
<nav>
    <ul>
        <li>
            <a href="../artist/dashboard">
                Dashboard home
            </a>
        </li>
        <li>
            <a href="../artist/artist/new">
                Add artist
            </a>
        </li>
    </ul>
</nav>
<main>
    <c:choose>
        <c:when test="${artist != null}">
            <h1><c:out value="${artist.name}"/></h1>
            <a href="<c:out value="${artist.id}"/>/tours" class="btn primary">
                Tours
            </a>
            <form method="post">
                <c:if test="${error != null}">
                    <div class="dialog error">
                        <p>
                            <c:out value="${error}"/>
                        </p>
                    </div>
                </c:if>
                <h3>
                    About you
                </h3>
                <div class="input">
                    <label for="name">
                        Stage name
                    </label>
                    <input type="text" name="name" id="name" value="<c:out value="${artist.name}"/>" required/>
                </div>
                <div class="input">
                    <label for="bio">
                        Tell us about yourself
                    </label>
                    <textarea name="bio" id="bio" maxlength="400" rows="3"><c:out value="${artist.bio}"/></textarea>
                </div>
                <div class="input checkbox">
                    <input type="checkbox" name="ontour" id="ontour"
                           <c:choose>
                               <c:when test="${artist.ontour == true}">checked</c:when>
                           </c:choose>
                    />
                    <label for="ontour">Are you currently touring?</label>
                </div>

                <h3>
                    Where can your fans find you?
                </h3>
                <div class="input">
                    <label for="instagram">
                        Your Instagram
                    </label>
                    <input type="text" id="instagram" name="instagram" value="<c:out value="${artist.artistSocialCollection[0].instagram}"/>"/>
                </div>
                <div class="input">
                    <label for="twitter">
                        Your Twitter
                    </label>
                    <input type="text" id="twitter" name="twitter" value="<c:out value="${artist.artistSocialCollection[0].twitter}"/>"/>
                </div>

                <h3>
                    Add some photos
                </h3>
                <p>Add at least one photo so your fans can easily recognize you on OnStage.</p>

                <div id="photos">
                    <c:set var="iteration" value="1"/>
                    <c:forEach var="image" items="${artist.artistImageCollection}">
                        <div class="input">
                            <label for="photo-url-<c:out value="${iteration}"/>">
                                Photo URL
                            </label>
                            <input value="${image.url}" type="url" id="photo-url-<c:out value="${iteration}"/>" name="photo-url-<c:out value="${iteration}"/>" required/>
                        </div>
                        <c:set var="iteration" value="${iteration+1}"/>
                    </c:forEach>
                </div>

                <div class="center">
                    <svg style="width:26px;height:26px" viewBox="0 0 24 24" onclick="addPhoto()">
                        <path fill="currentColor" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />
                    </svg>
                </div>

                <br/>

                <button type="submit" class="btn primary">
                    Save changes
                </button>

                <a class="btn danger" href="../artist/<c:out value="${artist.id}"/>/delete">
                    Delete
                </a>

            </form>
            <script>
                function addPhoto() {
                    let photoInputs = document.querySelectorAll("#photos > div");
                    if (photoInputs.length < 10) {
                        let photoContainer = document.querySelector("#photos");
                        let number = photoInputs.length + 1;
                        photoContainer.insertAdjacentHTML(
                            "beforeend",
                            '<div class="input">\n' +
                            '   <label for="photo-url-'+ number +'">\n' +
                            '      Photo URL\n' +
                            '   </label>\n' +
                            '   <input type="url" id="photo-url-'+ number +'" name="photo-url-'+ number +'"/>\n' +
                            '</div>'
                        );
                    }
                }
            </script>
        </c:when>
        <c:otherwise>
            <div class="dialog error">
                <p>
                    You do not have access to this page.
                </p>
            </div>
        </c:otherwise>
    </c:choose>
</main>
</body>
</html>
