<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>OnStage - Concerts near you</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <nav class="top">
        <ul>
            <li>
                <a href="tours/" title="Popular tours">Tours</a>
            </li>
            <li>
                <a href="shows/" title="Nearby shows">Shows</a>
            </li>
            <li>
                <a href="account/" title="OnStage account">Account</a>
            </li>
            <li>
                <input type="search" placeholder="Search artists, tours, shows"/>
            </li>
        </ul>
    </nav>
</body>
</html>