package com.realdolmen.onstage.RestController;

import com.realdolmen.onstage.beans.User;
import com.realdolmen.onstage.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserDao userDao;

    @GetMapping("/api/users/id")
    public User byId(
            @RequestParam(name = "id", required = true) long id
    ) {
        return userDao.findById(id);
    }
}
