package com.realdolmen.onstage.RestController;

import com.realdolmen.onstage.beans.PlannedShow;
import com.realdolmen.onstage.dao.PlannedShowDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PlannedShowController {
    @Autowired
    PlannedShowDao plannedShowDao;

    @GetMapping("/api/plannedshows/user")
    public List<PlannedShow> byUser(
            @RequestParam(name = "user", required = true) int userId
    ) {
        return plannedShowDao.findByUser(userId);
    }

    @PutMapping("/api/plannedshows/user")
    public boolean addForUser(
            @RequestParam(name = "plannedshow", required = true) PlannedShow ps
    ) {
        //TODO: same as with followedartistcontroller
        return false;
    }
}
