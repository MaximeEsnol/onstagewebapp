package com.realdolmen.onstage.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;
import com.realdolmen.onstage.beans.AuthKey;
import com.realdolmen.onstage.dao.AuthKeyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthKeyController {

    @Autowired
    AuthKeyDao authKeyDao;

    @JsonView(View.AuthKey.class)
    @GetMapping("/api/authkeys/selector")
    public AuthKey bySelector(
            @RequestParam(name = "selector", required = true) String selector
    ) {
        return authKeyDao.findBySelector(selector);
    }
}
