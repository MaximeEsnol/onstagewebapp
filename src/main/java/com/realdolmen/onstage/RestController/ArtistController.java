package com.realdolmen.onstage.RestController;

import com.realdolmen.onstage.beans.Artist;
import com.realdolmen.onstage.dao.ArtistDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ArtistController {

    @Autowired
    ArtistDao artistDao;

    @GetMapping("/api/artists/all")
    public List<Artist> all(
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return artistDao.findAll(page);
    }

    @GetMapping("/api/artists/id")
    public Artist byId(
            @RequestParam(name = "id", required = true) long id
    ) {
        return artistDao.findById(id);
    }

    @GetMapping("/api/artists/search")
    public List<Artist> search(
            @RequestParam(name = "search", required = true) String name,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return artistDao.findByNameLike(name, page);
    }

    @GetMapping("/api/artists/ontour")
    public List<Artist> touring(
            @RequestParam(name = "touring", defaultValue = "1") byte onTour,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return artistDao.findOnTour(onTour, page);
    }
}
