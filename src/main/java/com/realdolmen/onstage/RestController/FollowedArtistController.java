package com.realdolmen.onstage.RestController;

import com.realdolmen.onstage.beans.FollowedArtist;
import com.realdolmen.onstage.dao.FollowedArtistDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FollowedArtistController {

    @Autowired
    FollowedArtistDao followedArtistDao;

    @GetMapping("/api/followed/user")
    public List<FollowedArtist> byUser(
            @RequestParam(name = "userId", required = true) int userId,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        return followedArtistDao.findByUser(userId, page);
    }

    @PutMapping("/api/followed/user")
    public boolean follow(
            @RequestParam(name = "artistId", required = true) int artistId
    ) {
        //TODO: get current session's user ID, then get the artist, then create a FollowedArtist object to persist
        return false;
    }
}
