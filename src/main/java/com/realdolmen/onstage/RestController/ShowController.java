package com.realdolmen.onstage.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;
import com.realdolmen.onstage.beans.Show;
import com.realdolmen.onstage.dao.ShowDao;
import com.realdolmen.onstage.enums.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ShowController {

    @Autowired
    private ShowDao showDao;

    @GetMapping("/api/shows/all")
    @JsonView(View.Show.class)
    public List<Show> allShows(
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "order", required = true) Orders order
    ) {
        List<Show> shows = new ArrayList<>();
        shows = showDao.findAll(page, order);
        return shows;
    }

    @GetMapping("/api/shows/id")
    @JsonView(View.Show.class)
    public Show byId(
            @RequestParam(name = "id", required = true) int id
    ) {
        Show show = showDao.findById(id);
        return show;
    }

    @GetMapping("/api/shows/tour")
    @JsonView(View.Show.class)
    public List<Show> byTour(
            @RequestParam(name = "tourId", required = true) int tourId,
            @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        List<Show> shows = showDao.findByTour(tourId, page);
        return shows;
    }

    @GetMapping("/api/shows/nearby")
    @JsonView(View.Show.class)
    public List<Show> nearby(
            @RequestParam(name = "latitude", required = true) double latitude,
            @RequestParam(name = "longitude", required = true) double longitude,
            @RequestParam(name = "page", defaultValue = "1") int page
    ){
        List<Show> shows = showDao.findNearby(latitude, longitude, page);
        return shows;
    }

}
