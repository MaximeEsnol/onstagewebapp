package com.realdolmen.onstage.enums;

public enum Orders {
    NONE,
    ID,
    ID_DESC,
    PRICE,
    PRICE_DESC,
    NEARBY
}
