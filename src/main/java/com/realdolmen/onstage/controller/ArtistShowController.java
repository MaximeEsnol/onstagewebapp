package com.realdolmen.onstage.controller;

import com.realdolmen.onstage.beans.Show;
import com.realdolmen.onstage.beans.Tour;
import com.realdolmen.onstage.beans.User;
import com.realdolmen.onstage.dao.ArtistDao;
import com.realdolmen.onstage.dao.ShowDao;
import com.realdolmen.onstage.dao.TourDao;
import com.realdolmen.onstage.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class ArtistShowController {

    @Autowired
    ShowDao showDao;

    @Autowired
    TourDao tourDao;

    @Autowired
    ArtistDao artistDao;

    @Autowired
    UserDao userDao;

    @GetMapping("/artist/tour/{id}/shows")
    public ModelAndView tourShows(
            @PathVariable int id,
            @RequestParam(name = "message", defaultValue = "") String message,
            @RequestParam(name = "error", defaultValue = "") String error,
            HttpSession session,
            ModelMap map
    ) {
        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            Tour tour = tourDao.findById(id);
            if(tour.getArtist().getUser().getId() == user.getId()){
                List<Show> shows = showDao.findByTour(tour.getId(), 1);
                map.addAttribute("shows", shows);
                switch(message){
                    case "added":
                        map.addAttribute("message", "A new show has been added.");
                        break;
                    case "deleted":
                        map.addAttribute("message", "A show has been deleted.");
                        break;
                }

                switch(error){
                    case "deleteFailed":
                        map.addAttribute("error", "A show could not be deleted.");
                        break;
                }

                return new ModelAndView("/WEB-INF/dashboard/shows/home.jsp");
            }
            map.addAttribute("tour", tour);
            return new ModelAndView("redirect:/artist/artist/" + tour.getArtist().getId());
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/tour/{id}/show/add")
    public ModelAndView addShowForm(
            @PathVariable int id,
            HttpSession session,
            @RequestParam(name = "error", defaultValue = "") String error,
            ModelMap map
    ) {
        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            Tour tour = tourDao.findById(id);
            if(tour.getArtist().getUser().getId() == user.getId()){
                switch(error){
                    case "failed":
                        map.addAttribute("error", "Unable to add a new show to this tour. Please try again later.");
                        break;
                }
                map.addAttribute("tour", tour);
                return new ModelAndView("/WEB-INF/dashboard/shows/add.jsp");
            }
            return new ModelAndView("redirect:/artist/artist/" + tour.getArtist().getId());
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/show/{id}")
    public ModelAndView editShowForm(
            @PathVariable int id,
            HttpSession session,
            @RequestParam(name = "error", defaultValue = "") String error,
            @RequestParam(name = "message", defaultValue = "") String message,
            ModelMap map
    ) {
        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            Show show = showDao.findById(id);
            if(show.getTour().getArtist().getUser().getId() == user.getId()){
                switch(error){
                    case "failed":
                        map.addAttribute("error", "Unable to add a new show to this tour. Please try again later.");
                        break;
                }

                switch(message){
                    case "edited":
                        map.addAttribute("message", "Edited this show successfully.");
                        break;
                }
                map.addAttribute("show", show);
                SimpleDateFormat formatter = new SimpleDateFormat("EE MMM d y H:m:s ZZZ");
                LocalDate localDate = show.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDateTime localTime = show.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

                int year = localDate.getYear();
                int month = localDate.getMonthValue();
                int day = localDate.getDayOfMonth();

                int hour = localTime.getHour();
                int minute = localTime.getMinute();

                String m, d, h, min;

                if(month < 10){
                    m = "0" + month;
                } else {
                    m = Integer.toString(month);
                }

                if(day < 10){
                    d = "0" + day;
                } else {
                    d = Integer.toString(day);
                }

                if(hour < 10){
                    h = "0" + hour;
                } else {
                    h = Integer.toString(hour);
                }

                if(minute < 10){
                    min = "0" + minute;
                } else {
                    min = Integer.toString(minute);
                }

                map.addAttribute("date", year + "-" + m + "-" + d + " " + h + ":" + min);

                return new ModelAndView("/WEB-INF/dashboard/shows/edit.jsp");
            }
            return new ModelAndView("redirect:/artist/artist/" + show.getTour().getArtist().getId());
        }
        return new ModelAndView("redirect:/signin");
    }

    @PostMapping("/artist/show/{id}")
    public ModelAndView editShow(
            @PathVariable int id,
            HttpSession session,
            @RequestParam Map<String, String> params
    ) {
        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            Show show = showDao.findById(id);
            if(show.getTour().getArtist().getUser().getId() == user.getId()){
                Show generatedShow = makeShowFromInput(params, show.getTour());
                generatedShow.setId(id);
                generatedShow.setAvailability("good");
                if(showDao.edit(generatedShow)){
                    return new ModelAndView("redirect:/artist/show/"+id+"?message=edited");
                }
                return new ModelAndView("redirect:/artist/show/" + id + "?error=failed");
            }
            return new ModelAndView("redirect:/artist/artist/" + show.getTour().getArtist().getId());
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/show/{id}/delete")
    public ModelAndView deleteShow(
            @PathVariable int id,
            HttpSession session
    ) {
        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            Show show = showDao.findById(id);
            if(show.getTour().getArtist().getUser().getId() == user.getId()){
                if(showDao.delete(show.getId())){
                    return new ModelAndView("redirect:/artist/tour/" + show.getTour().getId() + "/shows?message=deleted");
                }
                return new ModelAndView("redirect:/artist/tour/" + show.getTour().getId() + "/shows?error=deleteFailed");
            }
            return new ModelAndView("redirect:/artist/artist/" + show.getTour().getArtist().getId());
        }
        return new ModelAndView("redirect:/signin");
    }

    @PostMapping("/artist/tour/{id}/show/add")
    public ModelAndView addShow(
            @PathVariable int id,
            HttpSession session,
            @RequestParam Map<String, String> params,
            ModelMap map
    ) {
        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            Tour tour = tourDao.findById(id);
            if(tour.getArtist().getUser().getId() == user.getId()){
                Show generatedShow = makeShowFromInput(params, tour);
                generatedShow.setAvailability("good");
                if(showDao.add(generatedShow)){
                    return new ModelAndView("redirect:/artist/tour/" + id + "/shows?message=added");
                }
                return new ModelAndView("redirect:/artist/tour/" + id + "/show/add?error=failed");
            }
            return new ModelAndView("redirect:/artist/artist/" + tour.getArtist().getId());
        }
        return new ModelAndView("redirect:/signin");
    }

    private Show makeShowFromInput(Map<String, String> params, Tour tour) {
        if(params.containsKey("location")
        && params.containsKey("venue")
        && params.containsKey("coords")
        && params.containsKey("datetime")
        && params.containsKey("ticketing")
        && params.containsKey("currency")
        && params.containsKey("min")
        && params.containsKey("max")){
            BigDecimal min, max;
            Date date;
            Date time;
            String currency;

            Show show = new Show();
            show.setLocation(params.get("location"));
            show.setVenue(params.get("venue"));
            show.setCoords(params.get("coords"));
            show.setTicketing(params.get("ticketing"));

            switch(params.get("currency")){
                case "USD":
                    currency = "USD";
                    break;
                case "GBP":
                    currency = "GBP";
                    break;
                case "EUR":
                default:
                    currency = "EUR";
                    break;
            }

            show.setCurrency(currency);

            min = new BigDecimal(params.get("min"));
            max = new BigDecimal(params.get("max"));

            show.setMin(min);
            show.setMax(max);

            try {
                date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(params.get("datetime"));

            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }

            show.setDate(date);
            show.setTime(date);
            show.setTour(tour);

            return show;
        }
        return null;
    }
}
