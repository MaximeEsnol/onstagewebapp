package com.realdolmen.onstage.controller;

import com.realdolmen.onstage.beans.Artist;
import com.realdolmen.onstage.beans.Tour;
import com.realdolmen.onstage.beans.User;
import com.realdolmen.onstage.dao.ArtistDao;
import com.realdolmen.onstage.dao.TourDao;
import com.realdolmen.onstage.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
public class ArtistTourController {

    @Autowired
    UserDao userDao;

    @Autowired
    TourDao tourDao;

    @Autowired
    ArtistDao artistDao;

    @GetMapping("/artist/{id}/tours")
    public ModelAndView artistTours(
            @PathVariable int id,
            @RequestParam(name = "message", defaultValue = "") String message,
            ModelMap map,
            HttpSession httpSession
    ) {
        if (httpSession.getAttribute("user") != null) {
            User user = (User) httpSession.getAttribute("user");
            Artist artist = artistDao.byIdFromUser(id, user.getId());
            if (artist != null) {
                List<Tour> tours = tourDao.findByArtist(artist.getId(), 1);
                map.addAttribute("tours", tours);
                map.addAttribute("artist", artist);

                switch(message){
                    case "deleted":
                        map.addAttribute("message", "A tour has been deleted.");
                        break;
                    case "added":
                        map.addAttribute("message", "A new tour has been added.");
                        break;
                }

                return new ModelAndView("/WEB-INF/dashboard/tour/home.jsp");
            }
            return new ModelAndView("redirect:/artist/" + id);
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/{id}/tour/add")
    public ModelAndView addTourForm(
            @PathVariable int id,
            ModelMap map,
            HttpSession httpSession
    ) {
        if (httpSession.getAttribute("user") != null) {
            User user = (User) httpSession.getAttribute("user");
            Artist artist = artistDao.byIdFromUser(id, user.getId());
            if (artist != null) {
                map.addAttribute("artist", artist);
                return new ModelAndView("/WEB-INF/dashboard/tour/add.jsp");
            }
            return new ModelAndView("redirect:/artist/" + id);
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/tour/{id}")
    public ModelAndView tourEditPage(
            @PathVariable int id,
            @RequestParam(name = "message", defaultValue = "") String message,
            @RequestParam(name = "error", defaultValue = "") String error,
            ModelMap map,
            HttpSession httpSession
    ) {
        if (httpSession.getAttribute("user") != null) {
            User user = (User) httpSession.getAttribute("user");
            Tour tour = tourDao.findById(id);
            if (tour.getArtist().getUser().getId() == user.getId()) {
                switch (error) {
                    case "failed":
                        map.addAttribute("error", "The tour could not be edited.");
                        break;
                }

                switch (message) {
                    case "success":
                        map.addAttribute("message", "The tour has been edited.");
                        break;
                }
                map.addAttribute("tour", tour);
                return new ModelAndView("/WEB-INF/dashboard/tour/edit.jsp");
            }
            return new ModelAndView("redirect:/artist/" + id);
        }
        return new ModelAndView("redirect:/signin");
    }

    @GetMapping("/artist/tour/{id}/delete")
    public ModelAndView deleteTour(
            @PathVariable int id,
            HttpSession session
    ) {
        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            Tour tour = tourDao.findById(id);
            if(tour.getArtist().getUser().getId() == user.getId()) {
                if(tourDao.delete(id)){
                    return new ModelAndView("redirect:/artist/" + tour.getArtist().getId() + "/tours?message=deleted");
                }
                return new ModelAndView("redirect:/artist/tour/" + id + "?error=failed");
            }
            return new ModelAndView("redirect:/artist/" + tour.getArtist().getId());
        }
        return new ModelAndView("redirect:/signin");
    }

    @PostMapping("/artist/tour/{id}")
    public ModelAndView editTour(
            @PathVariable int id,
            @RequestParam Map<String, String> params,
            ModelMap map,
            HttpSession session
    ) {
        if (session.getAttribute("user") != null) {
            User user = (User) session.getAttribute("user");
            Tour tour = tourDao.findById(id);
            if (tour.getArtist().getUser().getId() == user.getId()) {
                Tour generatedTour = handleTourUpload(params, tour.getArtist());
                if (generatedTour != null) {
                    generatedTour.setId(id);
                    if (tourDao.edit(generatedTour)) {
                        return new ModelAndView("redirect:/artist/tour/" + id + "?message=success");
                    }
                    return new ModelAndView("redirect:/artist/tour/" + id + "?error=failed");
                }
                return new ModelAndView("redirect:/artist/tour/" + id);
            }
            return new ModelAndView("redirect:/artist/tour/" + id);
        }
        return new ModelAndView("redirect:/signin");
    }

    @PostMapping("/artist/{id}/tour/add")
    public ModelAndView addTour(
            @PathVariable int id,
            @RequestParam Map<String, String> params,
            ModelMap map,
            HttpSession session
    ) {
        if (session.getAttribute("user") != null) {
            User user = (User) session.getAttribute("user");
            Artist artist = artistDao.byIdFromUser(id, user.getId());

            if (artist != null) {
                Tour generatedTour = handleTourUpload(params, artist);
                if (generatedTour != null) {
                    if (tourDao.add(generatedTour) != null) {
                        return new ModelAndView("redirect:/artist/" + id + "/tours?message=added");
                    }
                }
                return new ModelAndView("redirect:/artist/" + id + "/tours?error=failed");
            }
            return new ModelAndView("redirect:/artist/" + id);
        }
        return new ModelAndView("redirect:/signin");
    }

    private Tour handleTourUpload(Map<String, String> params, Artist artist) {
        if (params.containsKey("name")) {
            String name = params.get("name");
            String description = "";

            if (params.containsKey("description")) {
                description = params.get("description");
            }

            Tour tour = new Tour();
            tour.setName(name);
            tour.setDescription(description);
            tour.setArtist(artist);

            return tour;
        }
        return null;
    }
}
