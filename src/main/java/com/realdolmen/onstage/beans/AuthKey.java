package com.realdolmen.onstage.beans;

import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "auth_keys")
@NamedQueries({
        @NamedQuery(name = "AuthKeys.findAll", query = "SELECT a FROM AuthKey a"),
        @NamedQuery(name = "AuthKeys.findById", query = "SELECT a FROM AuthKey a WHERE a.id = :id"),
        @NamedQuery(name = "AuthKeys.findBySelector", query = "SELECT a FROM AuthKey a WHERE a.selector = :selector"),
        @NamedQuery(name = "AuthKeys.findByValidator", query = "SELECT a FROM AuthKey a WHERE a.validator = :validator"),
        @NamedQuery(name = "AuthKeys.findByExpiring", query = "SELECT a FROM AuthKey a WHERE a.expiring = :expiring")})
public class AuthKey {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "selector")
    @JsonView(View.AuthKey.class)
    private String selector;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "validator")
    @JsonView(View.AuthKey.class)
    private String validator;
    @Basic(optional = false)
    @NotNull
    @Column(name = "expiring")
    @JsonView(View.AuthKey.class)
    private long expiring;
    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonView(View.AuthKey.class)
    private User user;

    public AuthKey() {
    }

    public AuthKey(Integer id) {
        this.id = id;
    }

    public AuthKey(Integer id, String selector, String validator, long expiring) {
        this.id = id;
        this.selector = selector;
        this.validator = validator;
        this.expiring = expiring;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public String getValidator() {
        return validator;
    }

    public void setValidator(String validator) {
        this.validator = validator;
    }

    public long getExpiring() {
        return expiring;
    }

    public void setExpiring(long expiring) {
        this.expiring = expiring;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuthKey)) {
            return false;
        }
        AuthKey other = (AuthKey) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.onstage.AuthKeys[ id=" + id + " ]";
    }
}
