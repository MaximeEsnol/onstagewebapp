package com.realdolmen.onstage.beans;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.realdolmen.onstage.View.View;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "planned_shows")
@NamedQueries({
        @NamedQuery(name = "PlannedShow.findAll", query = "SELECT p FROM PlannedShow p"),
        @NamedQuery(name = "PlannedShow.findByUser", query = "SELECT p FROM PlannedShow p WHERE p.userId.id = :userId"),
        @NamedQuery(name = "PlannedShow.findById", query = "SELECT p FROM PlannedShow p WHERE p.id = :id")})
public class PlannedShow implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @JsonView(View.PlannedShow.class)
    private Integer id;
    @JoinColumn(name = "show_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonView(View.PlannedShow.class)
    private Show showId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonView(View.PlannedShow.class)
    private User userId;

    public PlannedShow() {
    }

    public PlannedShow(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Show getShowId() {
        return showId;
    }

    public void setShowId(Show showId) {
        this.showId = showId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlannedShow)) {
            return false;
        }
        PlannedShow other = (PlannedShow) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.onstage.PlannedShow[ id=" + id + " ]";
    }

}
