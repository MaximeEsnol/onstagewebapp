package com.realdolmen.onstage.beans;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "shows")
@NamedQueries({
        @NamedQuery(name = "Show.findAll", query = "SELECT s FROM Show s"),
        @NamedQuery(name = "Show.findByTour", query = "SELECT s FROM Show s WHERE s.tour.id = :tourId"),
        @NamedQuery(name = "Show.findAllOrderByIdDesc", query = "SELECT s FROM Show s ORDER BY s.id DESC"),
        @NamedQuery(name = "Show.findAllOrderByPrice", query = "SELECT s FROM Show s ORDER BY s.min"),
        @NamedQuery(name = "Show.findAllOrderByPriceDesc", query = "SELECT s FROM Show s ORDER BY s.min DESC"),
        @NamedQuery(name = "Show.findById", query = "SELECT s FROM Show s WHERE s.id = :id"),
        @NamedQuery(name = "Show.findByLocation", query = "SELECT s FROM Show s WHERE s.location = :location"),
        @NamedQuery(name = "Show.findByCoords", query = "SELECT s FROM Show s WHERE s.coords = :coords"),
        @NamedQuery(name = "Show.findByVenue", query = "SELECT s FROM Show s WHERE s.venue = :venue"),
        @NamedQuery(name = "Show.findByDate", query = "SELECT s FROM Show s WHERE s.date = :date"),
        @NamedQuery(name = "Show.findByTime", query = "SELECT s FROM Show s WHERE s.time = :time"),
        @NamedQuery(name = "Show.findByAvailability", query = "SELECT s FROM Show s WHERE s.availability = :availability"),
        @NamedQuery(name = "Show.findByCurrency", query = "SELECT s FROM Show s WHERE s.currency = :currency"),
        @NamedQuery(name = "Show.findByMin", query = "SELECT s FROM Show s WHERE s.min = :min"),
        @NamedQuery(name = "Show.findByMax", query = "SELECT s FROM Show s WHERE s.max = :max")})
public class Show implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @JsonView({View.Show.class, View.PlannedShow.class})
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "location")
    @JsonView(View.Show.class)
    private String location;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "coords")
    @JsonView(View.Show.class)
    private String coords;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "venue")
    @JsonView(View.Show.class)
    private String venue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @JsonView(View.Show.class)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time")
    @Temporal(TemporalType.TIME)
    @JsonView(View.Show.class)
    private Date time;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "availability")
    @JsonView(View.Show.class)
    private String availability;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "currency")
    @JsonView(View.Show.class)
    private String currency;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "min")
    @JsonView(View.Show.class)
    private BigDecimal min;
    @Basic(optional = false)
    @NotNull
    @Column(name = "max")
    @JsonView(View.Show.class)
    private BigDecimal max;
    @Lob
    @Size(max = 65535)
    @Column(name = "ticketing")
    @JsonView(View.Show.class)
    private String ticketing;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "showId", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<PlannedShow> plannedShowCollection;
    @JoinColumn(name = "tour", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonView(View.Show.class)
    private Tour tour;

    public Show() {
    }

    public Show(Integer id) {
        this.id = id;
    }

    public Show(Integer id, String location, String coords, String venue, Date date, Date time, String availability, String currency, BigDecimal min, BigDecimal max) {
        this.id = id;
        this.location = location;
        this.coords = coords;
        this.venue = venue;
        this.date = date;
        this.time = time;
        this.availability = availability;
        this.currency = currency;
        this.min = min;
        this.max = max;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCoords() {
        return coords;
    }

    public void setCoords(String coords) {
        this.coords = coords;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getMin() {
        return min;
    }

    public void setMin(BigDecimal min) {
        this.min = min;
    }

    public BigDecimal getMax() {
        return max;
    }

    public void setMax(BigDecimal max) {
        this.max = max;
    }

    public String getTicketing() {
        return ticketing;
    }

    public void setTicketing(String ticketing) {
        this.ticketing = ticketing;
    }

    public Collection<PlannedShow> getPlannedShowCollection() {
        return plannedShowCollection;
    }

    public void setPlannedShowCollection(Collection<PlannedShow> plannedShowCollection) {
        this.plannedShowCollection = plannedShowCollection;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Show)) {
            return false;
        }
        Show other = (Show) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.onstage.Show[ id=" + id + " ]";
    }

}
