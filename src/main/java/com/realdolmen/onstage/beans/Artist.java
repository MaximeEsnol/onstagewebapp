package com.realdolmen.onstage.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "artists")
@NamedQueries({
        @NamedQuery(name = "Artist.findAll", query = "SELECT a FROM Artist a"),
        @NamedQuery(name = "Artist.findById", query = "SELECT a FROM Artist a WHERE a.id = :id"),
        @NamedQuery(name = "Artist.findByName", query = "SELECT a FROM Artist a WHERE a.name = :name"),
        @NamedQuery(name = "Artist.findByNameLike", query = "SELECT a FROM Artist a WHERE a.name LIKE :name"),
        @NamedQuery(name = "Artist.findByOnTour", query = "SELECT a FROM Artist a WHERE a.ontour = :ontour"),
        @NamedQuery(name = "Artist.findByBio", query = "SELECT a FROM Artist a WHERE a.bio = :bio"),
        @NamedQuery(name = "Artist.isFromUser", query = "SELECT a FROM Artist a WHERE a.id = :id AND a.user.id = :userId"),
        @NamedQuery(name = "Artist.findByUser", query = "SELECT a FROM Artist a WHERE a.user.id = :userId")
        })
public class Artist implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @JsonView({View.Tour.class, View.FollowedArtist.class})
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 400)
    @Column(name = "bio")
    private String bio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ontour")
    private boolean ontour;
    @ManyToOne
    @JoinColumn(name = "user")
    private User user;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artistId")
    @JsonIgnore
    private Collection<FollowedArtist> followedArtistCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artistId", fetch = FetchType.EAGER)
    private Collection<ArtistImage> artistImageCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artistId", fetch = FetchType.EAGER)
    private Collection<ArtistSocial> artistSocialCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artist")
    @JsonIgnore
    private Collection<Tour> tourCollection;

    public Artist() {
    }

    public Artist(Integer id) {
        this.id = id;
    }

    public Artist(Integer id, String name, String bio, boolean ontour) {
        this.id = id;
        this.name = name;
        this.bio = bio;
        this.ontour = ontour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public boolean getOntour() {
        return ontour;
    }

    public void setOntour(boolean ontour) {
        this.ontour = ontour;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collection<FollowedArtist> getFollowedArtistCollection() {
        return followedArtistCollection;
    }

    public void setFollowedArtistCollection(Collection<FollowedArtist> followedArtistCollection) {
        this.followedArtistCollection = followedArtistCollection;
    }

    public Collection<ArtistImage> getArtistImageCollection() {
        return artistImageCollection;
    }

    public void setArtistImageCollection(Collection<ArtistImage> artistImageCollection) {
        this.artistImageCollection = artistImageCollection;
    }

    public Collection<ArtistSocial> getArtistSocialCollection() {
        return artistSocialCollection;
    }

    public void setArtistSocialCollection(Collection<ArtistSocial> artistSocialCollection) {
        this.artistSocialCollection = artistSocialCollection;
    }

    public Collection<Tour> getTourCollection() {
        return tourCollection;
    }

    public void setTourCollection(Collection<Tour> tourCollection) {
        this.tourCollection = tourCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Artist)) {
            return false;
        }
        Artist other = (Artist) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.onstage.Artist[ id=" + id + " ]";
    }

}
