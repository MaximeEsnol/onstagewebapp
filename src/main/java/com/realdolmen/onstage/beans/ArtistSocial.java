package com.realdolmen.onstage.beans;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "artist_socials")
@NamedQueries({
        @NamedQuery(name = "ArtistSocial.findAll", query = "SELECT a FROM ArtistSocial a"),
        @NamedQuery(name = "ArtistSocial.findById", query = "SELECT a FROM ArtistSocial a WHERE a.id = :id"),
        @NamedQuery(name = "ArtistSocial.findByInstagram", query = "SELECT a FROM ArtistSocial a WHERE a.instagram = :instagram"),
        @NamedQuery(name = "ArtistSocial.findByTwitter", query = "SELECT a FROM ArtistSocial a WHERE a.twitter = :twitter")})
public class ArtistSocial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "instagram")
    private String instagram;
    @Size(max = 15)
    @Column(name = "twitter")
    private String twitter;
    @JoinColumn(name = "artist_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Artist artistId;

    public ArtistSocial() {
    }

    public ArtistSocial(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public Artist getArtistId() {
        return artistId;
    }

    public void setArtistId(Artist artistId) {
        this.artistId = artistId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArtistSocial)) {
            return false;
        }
        ArtistSocial other = (ArtistSocial) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.onstage.ArtistSocial[ id=" + id + " ]";
    }

}
