package com.realdolmen.onstage.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "tours")
@NamedQueries({
        @NamedQuery(name = "Tour.findAll", query = "SELECT t FROM Tour t"),
        @NamedQuery(name = "Tour.findByNameLike", query = "SELECT t FROM Tour t WHERE t.name LIKE :name"),
        @NamedQuery(name = "Tour.findByArtist", query = "SELECT t FROM Tour t WHERE t.artist.id = :artistId"),
        @NamedQuery(name = "Tour.findById", query = "SELECT t FROM Tour t WHERE t.id = :id"),
        @NamedQuery(name = "Tour.findByName", query = "SELECT t FROM Tour t WHERE t.name = :name"),
        @NamedQuery(name = "Tour.findByDescription", query = "SELECT t FROM Tour t WHERE t.description = :description"),
        @NamedQuery(name = "Tour.findByTopPick", query = "SELECT t FROM Tour t WHERE t.topPick = :topPick")})
public class Tour implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @JsonView({View.Tour.class, View.Show.class})
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "name")
    @JsonView(View.Tour.class)
    private String name;
    @Size(max = 400)
    @Column(name = "description")
    @JsonView(View.Tour.class)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "top_pick")
    @JsonView(View.Tour.class)
    private boolean topPick;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tour", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Show> showCollection;
    @JoinColumn(name = "artist", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonView(View.Tour.class)
    private Artist artist;

    public Tour() {
    }

    public Tour(Integer id) {
        this.id = id;
    }

    public Tour(Integer id, String name, boolean topPick) {
        this.id = id;
        this.name = name;
        this.topPick = topPick;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getTopPick() {
        return topPick;
    }

    public void setTopPick(boolean topPick) {
        this.topPick = topPick;
    }

    public Collection<Show> getShowCollection() {
        return showCollection;
    }

    public void setShowCollection(Collection<Show> showCollection) {
        this.showCollection = showCollection;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tour)) {
            return false;
        }
        Tour other = (Tour) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.onstage.Tour[ id=" + id + " ]";
    }

}
