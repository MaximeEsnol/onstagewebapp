package com.realdolmen.onstage.beans;


import com.fasterxml.jackson.annotation.JsonView;
import com.realdolmen.onstage.View.View;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "followed_artists")
@NamedQueries({
        @NamedQuery(name = "FollowedArtist.findAll", query = "SELECT f FROM FollowedArtist f"),
        @NamedQuery(name = "FollowedArtist.findByUser", query = "SELECT f FROM FollowedArtist f WHERE f.userId.id = :userId"),
        @NamedQuery(name = "FollowedArtist.findById", query = "SELECT f FROM FollowedArtist f WHERE f.id = :id")})
public class FollowedArtist implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @JsonView(View.FollowedArtist.class)
    private Integer id;
    @JoinColumn(name = "artist_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonView(View.FollowedArtist.class)
    private Artist artistId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonView(View.FollowedArtist.class)
    private User userId;

    public FollowedArtist() {
    }

    public FollowedArtist(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Artist getArtistId() {
        return artistId;
    }

    public void setArtistId(Artist artistId) {
        this.artistId = artistId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FollowedArtist)) {
            return false;
        }
        FollowedArtist other = (FollowedArtist) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.onstage.FollowedArtist[ id=" + id + " ]";
    }

}
