package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.PlannedShow;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class PlannedShowDao extends Dao{
    public PlannedShow findById(int id){
        em = jpaUtil.createEntityManager();
        TypedQuery<PlannedShow> query = em.createNamedQuery("PlannedShow.findById", PlannedShow.class);
        query.setParameter("id", id);
        PlannedShow ps;
        try{
            ps = query.getSingleResult();
        } catch (NoResultException e){
            ps = null;
        }
        em.close();
        return ps;
    }

    public List<PlannedShow> findByUser(int userId){
        em = jpaUtil.createEntityManager();
        TypedQuery<PlannedShow> query = em.createNamedQuery("PlannedShow.findByUser", PlannedShow.class);
        query.setParameter("userId", userId);
        List<PlannedShow> ps;
        try{
            ps = query.getResultList();
        } catch (NoResultException e){
            ps = new ArrayList<>();
        }
        em.close();
        return ps;
    }

    public boolean add(PlannedShow ps){
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(ps);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        PlannedShow ps = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(ps)){
                ps = em.merge(ps);
            }
            em.remove(ps);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }
}
