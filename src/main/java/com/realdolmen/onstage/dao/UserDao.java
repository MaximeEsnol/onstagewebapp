package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserDao extends Dao{
    public User findById(long id){
        em = jpaUtil.createEntityManager();
        TypedQuery<User> query = em.createNamedQuery("User.findById", User.class);
        query.setParameter("id", id);
        User user;
        try{
            user = query.getSingleResult();
        } catch (NoResultException e){
            user = null;
        }
        em.close();
        return user;
    }

    public List<User> findByUsernameLike(String username, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<User> query = em.createNamedQuery("User.findByUsernameLike", User.class);
        query.setParameter("username", username);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<User> users;
        try{
            users = query.getResultList();
        } catch (NoResultException e){
            users = new ArrayList<>();
        }
        em.close();
        return users;
    }

    public User findByEmail(String email){
        em = jpaUtil.createEntityManager();
        TypedQuery<User> query = em.createNamedQuery("User.findByEmail", User.class);
        query.setParameter("email", email);
        User user;
        try{
            user = query.getSingleResult();
        } catch (NoResultException e){
            user = null;
        }
        em.close();
        return user;
    }

    public User findByUsername(String username){
        em = jpaUtil.createEntityManager();
        TypedQuery<User> query = em.createNamedQuery("User.findByUsername", User.class);
        query.setParameter("username", username);
        User user;
        try{
            user = query.getSingleResult();
        } catch (NoResultException e){
            user = null;
        }
        em.close();
        return user;
    }

    public boolean add(User user){
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(user);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean edit(User user){
        User old = findById(user.getId());
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            old.setEmail(user.getEmail());
            old.setPassword(user.getPassword());
            old.setUsername(user.getUsername());
            em.merge(old);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        User old = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(old)){
                old = em.merge(old);
            }
            em.remove(old);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }
}
