package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.Tour;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class TourDao extends Dao{
    public List<Tour> findAll(int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Tour> query = em.createNamedQuery("Tour.findAll", Tour.class);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Tour> tours;
        try{
            tours = query.getResultList();
        } catch (NoResultException e){
            tours = new ArrayList<>();
        }
        em.close();
        return tours;
    }

    public Tour findById(long id){
        em = jpaUtil.createEntityManager();
        TypedQuery<Tour> query = em.createNamedQuery("Tour.findById", Tour.class);
        query.setParameter("id", id);
        Tour tour;
        try{
            tour = query.getSingleResult();
        } catch (NoResultException e){
            tour = null;
        }
        em.close();
        return tour;
    }

    public List<Tour> findByArtist(long artistId, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Tour> query = em.createNamedQuery("Tour.findByArtist", Tour.class);
        query.setParameter("artistId", artistId);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Tour> tours;
        try{
            tours = query.getResultList();
        } catch (NoResultException e){
            tours = new ArrayList<>();
        }
        em.close();
        return tours;
    }

    public List<Tour> findByNameLike(String name, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Tour> query = em.createNamedQuery("Tour.findByNameLike", Tour.class);
        query.setParameter("name", name);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Tour> tours;
        try{
            tours = query.getResultList();
        } catch (NoResultException e){
            tours = new ArrayList<>();
        }
        em.close();
        return tours;
    }

    public List<Tour> findByTopPick(boolean isTopPicked, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Tour> query = em.createNamedQuery("Tour.findByTopPick", Tour.class);
        query.setParameter("topPick", isTopPicked);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Tour> tours;
        try{
            tours = query.getResultList();
        } catch (NoResultException e){
            tours = new ArrayList<>();
        }
        em.close();
        return tours;
    }

    public Tour add(Tour tour){
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(tour);
            em.flush();
            tx.commit();
            return tour;
        } catch (RollbackException e){
            return null;
        } finally {
            em.close();
        }
    }

    public boolean edit(Tour tour){
        Tour old = findById(tour.getId());
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            old.setArtist(tour.getArtist());
            old.setName(tour.getName());
            old.setDescription(tour.getDescription());
            old.setTopPick(tour.getTopPick());
            old.setArtist(tour.getArtist());
            em.merge(old);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        Tour old = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(old)){
                old = em.merge(old);
            }
            em.remove(old);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

}
