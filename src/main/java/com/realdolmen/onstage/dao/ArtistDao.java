package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.Artist;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class ArtistDao extends Dao{
    public Artist findById(long id){
        em = jpaUtil.createEntityManager();
        TypedQuery<Artist> query = em.createNamedQuery("Artist.findById", Artist.class);
        query.setParameter("id", id);
        Artist artist;
        try{
            artist = query.getSingleResult();
        } catch (NoResultException e) {
            artist = null;
        }
        em.close();
        return artist;
    }

    public List<Artist> findAll(int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Artist> query = em.createNamedQuery("Artist.findAll", Artist.class);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Artist> artists;
        try{
            artists = query.getResultList();
        } catch (NoResultException e){
            artists = new ArrayList<>();
        }

        em.close();
        return artists;
    }

    public List<Artist> findByNameLike(String name, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Artist> query = em.createNamedQuery("Artist.findByNameLike", Artist.class);
        query.setParameter("name", "%" + name + "%");
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Artist> artists;
        try{
            artists = query.getResultList();
        } catch (NoResultException e){
            artists = new ArrayList<>();
        }
        em.close();
        return artists;
    }

    public List<Artist> findOnTour(byte onTour, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Artist> query = em.createNamedQuery("Artist.findByOnTour", Artist.class);
        query.setParameter("ontour", onTour);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Artist> artists;
        try{
            artists = query.getResultList();
        } catch (NoResultException e){
            artists = new ArrayList<>();
        }
        em.close();
        return artists;
    }

    public Artist byIdFromUser(int id, int userId){
        em = jpaUtil.createEntityManager();
        TypedQuery<Artist> q = em.createNamedQuery("Artist.isFromUser", Artist.class);
        q.setParameter("id", id);
        q.setParameter("userId", userId);
        Artist artist = new Artist();
        try{
            artist = q.getSingleResult();
        } catch (NoResultException e){
            artist = null;
        }
        em.close();
        return artist;
    }

    public List<Artist> findByUser(int userId, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<Artist> query = em.createNamedQuery("Artist.findByUser", Artist.class);
        query.setParameter("userId", userId);
        query.setFirstResult((page-1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Artist> artists;
        try{
            artists = query.getResultList();
        } catch (NoResultException e){
            artists = new ArrayList<>();
        }
        em.close();
        return artists;
    }

    public Artist add(Artist artist){
        em = jpaUtil.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try{
            tx.begin();
            em.persist(artist);
            em.flush();
            tx.commit();
            return artist;
        } catch (RollbackException ex){
            return null;
        } finally{
            em.close();
        }
    }

    public boolean edit(Artist artist){
        Artist old = findById(artist.getId());
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            old.setName(artist.getName());
            old.setBio(artist.getBio());
            old.setOntour(artist.getOntour());
            old.setArtistSocialCollection(artist.getArtistSocialCollection());
            old.setArtistImageCollection(artist.getArtistImageCollection());
            em.merge(old);
            tx.commit();
            return true;
        } catch (RollbackException ex){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        Artist artist = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(artist)){
                artist = em.merge(artist);
            }
            em.remove(artist);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally{
            em.close();
        }
    }

}
