package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.FollowedArtist;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class FollowedArtistDao extends Dao{
    public FollowedArtist findById(int id){
        em = jpaUtil.createEntityManager();
        TypedQuery<FollowedArtist> query = em.createNamedQuery("FollowedArtist.findById", FollowedArtist.class);
        query.setParameter("id", id);
        FollowedArtist fa;
        try{
            fa = query.getSingleResult();
        } catch (NoResultException e){
            fa = null;
        }
        em.close();
        return fa;
    }

    public List<FollowedArtist> findByUser(int userId, int page){
        em = jpaUtil.createEntityManager();
        TypedQuery<FollowedArtist> query = em.createNamedQuery("FollowedArtist.findByUser", FollowedArtist.class);
        query.setParameter("userId", userId);
        query.setFirstResult((page - 1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<FollowedArtist> fa;
        try{
            fa = query.getResultList();
        } catch (NoResultException e){
            fa = new ArrayList<>();
        }
        em.close();
        return fa;
    }

    public boolean add(FollowedArtist fa){
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(fa);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        FollowedArtist fa = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(fa)){
                fa = em.merge(fa);
            }
            em.remove(fa);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }
}
