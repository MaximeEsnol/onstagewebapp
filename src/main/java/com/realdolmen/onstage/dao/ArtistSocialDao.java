package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.ArtistSocial;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;

@Service
public class ArtistSocialDao extends Dao {
    public ArtistSocial findById(int id){
        em = jpaUtil.createEntityManager();
        TypedQuery<ArtistSocial> query = em.createNamedQuery("ArtistSocial.findById", ArtistSocial.class);
        query.setParameter("id", id);
        ArtistSocial as;
        try{
            as = query.getSingleResult();
        } catch (NoResultException e) {
            as = null;
        }
        em.close();
        return as;
    }

    public boolean add(ArtistSocial as){
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(as);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean edit(ArtistSocial as){
        ArtistSocial old = findById(as.getId());
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            old.setArtistId(as.getArtistId());
            old.setInstagram(as.getInstagram());
            old.setTwitter(as.getTwitter());
            em.merge(old);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        ArtistSocial as = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(as)){
                as = em.merge(as);
            }
            em.remove(as);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }
}
