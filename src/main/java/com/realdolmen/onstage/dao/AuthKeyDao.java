package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.AuthKey;
import org.springframework.stereotype.Service;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;

@Service
public class AuthKeyDao extends Dao {
    public AuthKey findById(int id){
        em = jpaUtil.createEntityManager();
        TypedQuery<AuthKey> q = em.createNamedQuery("AuthKeys.findById", AuthKey.class);
        q.setParameter("id", id);
        AuthKey ak;
        try{
            ak = q.getSingleResult();
        } catch (NoResultException e){
            ak = null;
        }
        em.close();
        return ak;
    }

    public AuthKey findBySelector(String selector){
        em = jpaUtil.createEntityManager();
        TypedQuery<AuthKey> q = em.createNamedQuery("AuthKeys.findBySelector", AuthKey.class);
        q.setParameter("selector", selector);
        AuthKey ak;
        try{
            ak = q.getSingleResult();
        } catch (NoResultException e){
            ak = null;
        }
        em.close();
        return ak;
    }

    public boolean add(AuthKey ak){
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(ak);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean edit(AuthKey ak){
        AuthKey old = findById(ak.getId());
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            old.setSelector(ak.getSelector());
            old.setValidator(ak.getValidator());
            old.setExpiring(ak.getExpiring());
            old.setUser(ak.getUser());
            em.merge(ak);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id){
        AuthKey old = findById(id);
        em = jpaUtil.createEntityManager();
        try{
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(old)){
                old = em.merge(old);
            }
            em.remove(old);
            tx.commit();
            return true;
        } catch (RollbackException e){
            return false;
        } finally {
            em.close();
        }
    }
}
