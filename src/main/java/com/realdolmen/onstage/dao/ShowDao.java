package com.realdolmen.onstage.dao;

import com.realdolmen.onstage.beans.Show;
import com.realdolmen.onstage.enums.Orders;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShowDao extends Dao {
    public List<Show> findAll(int page, Orders order) {
        em = jpaUtil.createEntityManager();
        TypedQuery<Show> query = getOrderByQuery(order);
        query.setFirstResult((page - 1) * this.maxResultsPerPage);
        query.setMaxResults(this.maxResultsPerPage);
        List<Show> shows;
        try{
            shows = query.getResultList();
        } catch (NoResultException e){
            shows = new ArrayList<>();
        }
        em.close();
        return shows;
    }

    private TypedQuery getOrderByQuery(Orders order) {
        switch (order) {
            case ID_DESC:
                return em.createNamedQuery("Show.findAllOrderByIdDesc", Show.class);
            case PRICE:
                return em.createNamedQuery("Show.findAllOrderByPrice", Show.class);
            case PRICE_DESC:
                return em.createNamedQuery("Show.findAllOrderByPriceDesc", Show.class);
            case NONE:
            case NEARBY:
            case ID:
            default:
                return em.createNamedQuery("Show.findAll", Show.class);
        }
    }

    public Show findById(int id) {
        em = jpaUtil.createEntityManager();
        TypedQuery<Show> query = em.createNamedQuery("Show.findById", Show.class);
        query.setParameter("id", id);
        Show show;
        try{
            show = query.getSingleResult();
        } catch (NoResultException e){
            show = null;
        }
        em.close();
        return show;
    }

    public List<Show> findByTour(int tourId, int page) {
        em = jpaUtil.createEntityManager();
        TypedQuery<Show> query = em.createNamedQuery("Show.findByTour", Show.class);
        query.setParameter("tourId", tourId);
        query.setMaxResults(this.maxResultsPerPage);
        query.setFirstResult((page - 1) * this.maxResultsPerPage);
        List<Show> shows;
        try{
            shows = query.getResultList();
        } catch (NoResultException e){
            shows = new ArrayList<>();
        }
        em.close();
        return shows;
    }

    public List<Show> findNearby(double latitude, double longitude, int page) {
        em = jpaUtil.createEntityManager();
        Query q = em.createNativeQuery(
                "SELECT *, SQRT(POW(SPLIT_STRING(coords, \",\", 1) - ?, 2) + POW( SPLIT_STRING(coords, \",\", 2) - ?, 2)) AS distance " +
                        "FROM `shows` " +
                        "HAVING distance <= 1.75 " +
                        "ORDER BY distance ASC", Show.class
        );
        q.setParameter(1, latitude);
        q.setParameter(2, longitude);
        q.setFirstResult((page-1) * this.maxResultsPerPage);
        q.setMaxResults(this.maxResultsPerPage);
        List<Show> shows;
        try{
            shows = q.getResultList();
        } catch (NoResultException e){
            shows = new ArrayList<>();
        }
        em.close();
        return shows;
    }

    public List<Show> findNearbyFromTour(double latitude, double longitude, int tourId, int page){
        em = jpaUtil.createEntityManager();
        Query q = em.createNativeQuery(
                "SELECT *, SQRT(POW(SPLIT_STRING(coords, \",\", 1) - ?, 2) + POW( SPLIT_STRING(coords, \",\", 2) - ?, 2)) AS distance " +
                        "FROM `shows` " +
                        "WHERE tour = ?" +
                        "HAVING distance <= 1.75 " +
                        "ORDER BY distance ASC", Show.class
        );
        q.setParameter(1, latitude);
        q.setParameter(2, longitude);
        q.setParameter(3, tourId);
        q.setFirstResult((page-1) * this.maxResultsPerPage);
        q.setMaxResults(this.maxResultsPerPage);
        List<Show> shows;
        try{
            shows = q.getResultList();
        } catch (NoResultException e){
            shows = new ArrayList<>();
        }
        em.close();
        return shows;
    }

    public boolean add(Show show) {
        em = jpaUtil.createEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(show);
            tx.commit();
            return true;
        } catch (RollbackException e) {
            return false;
        } finally {
            em.close();
        }
    }

    public boolean edit(Show show) {
        Show old = findById(show.getId());
        em = jpaUtil.createEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            old.setTour(show.getTour());
            old.setLocation(show.getLocation());
            old.setCoords(show.getCoords());
            old.setVenue(show.getVenue());
            old.setDate(show.getDate());
            old.setTime(show.getTime());
            old.setAvailability(show.getAvailability());
            old.setCurrency(show.getCurrency());
            old.setMax(show.getMax());
            old.setMin(show.getMin());
            old.setTicketing(show.getTicketing());
            em.merge(old);
            tx.commit();
            return true;
        } catch (RollbackException e) {
            return false;
        } finally {
            em.close();
        }
    }

    public boolean delete(int id) {
        Show old = findById(id);
        em = jpaUtil.createEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            if(!em.contains(old)){
                old = em.merge(old);
            }
            em.remove(old);
            tx.commit();
            return true;
        } catch (RollbackException e) {
            return false;
        } finally {
            em.close();
        }
    }
}
