package com.realdolmen.onstage.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
    private final EntityManagerFactory emf;
    private static JpaUtil jpaUtility;


    public void close() {
        emf.close();
    }

    public EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    private JpaUtil() {
        emf = Persistence.createEntityManagerFactory("OnStagePersistence");
    }

    public static JpaUtil getInstance() {
        if (jpaUtility == null) {
            jpaUtility = new JpaUtil();
        }
        return jpaUtility;
    }
}
