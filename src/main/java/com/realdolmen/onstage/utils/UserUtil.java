package com.realdolmen.onstage.utils;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.realdolmen.onstage.beans.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserUtil {

    public static boolean verifyUserFields(User user){
        if(verifyEmail(user.getEmail()) && verifyUsername(user.getUsername()) && verifyPasswordStrength(user.getPassword())){
            return true;
        }
        return false;
    }

    public static boolean verifyUsername(String username){
        if(username.matches("^[a-zA-Z0-9]{3,20}$")){
            return true;
        }
        return false;
    }

    public static boolean verifyPasswordStrength(String password){
        if(password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$")){
            return true;
        }
        return false;
    }

    public static boolean verifyEmail(String email){
        Pattern pattern = Pattern.compile("^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public static String hashPassword(String password){
        return BCrypt.withDefaults().hashToString(6, password.toCharArray());
    }

    public static boolean verifyPassword(String passwordPlain, String passwordHashed){
        return BCrypt.verifyer().verify(passwordPlain.toCharArray(), passwordHashed.toCharArray()).verified;
    }

}
